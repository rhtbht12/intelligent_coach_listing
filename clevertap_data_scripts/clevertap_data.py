import requests
import json
import argparse
import os
headers = {
    'X-CleverTap-Account-Id': '6ZR-75W-885Z',
    'X-CleverTap-Passcode': '348497641-250315-1609321280',
    'Content-Type': 'application/json',
}

params = (
    ('app',False),('event',False),('profile',False)
)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Clevertap data gathering')
    parser.add_argument('--event', type=str)
    parser.add_argument('--fromdate', type=str)
    parser.add_argument('--todate', type=str)
    parser.add_argument('--output_dir', type=str,default='app_launched')
    args = parser.parse_args()

    if args.output_dir:
        if not os.path.exists(args.output_dir):
            os.makedirs(args.output_dir)

    args.event = "App Launched"
    args.fromdate = '20200701'
    args.todate = '20200801'

    data = '{"event_name":"' + str(args.event) + '","from":'+ str(args.fromdate) + ',"to":' +str(args.todate) + '}'

    response = requests.post('https://api.clevertap.com/1/events.json', headers=headers,params=params,data=data)
    data = json.loads(response.content)
    cursor = data['cursor']
    final_data = None
    count = 1
    while cursor != None:

        eventdata = requests.get('https://api.clevertap.com/1/events.json?cursor=' + str(cursor), headers=headers)
        print("-------ping------")
        file = json.loads(eventdata.content)
        if 'next_cursor' not in file.keys():
            cursor = None
        else:
            if final_data == None:
                final_data = file
            else:
                final_data['records'].extend(file['records'])
            cursor = file['next_cursor']
        
    print("------")
    filename = args.output_dir + '/clevertap_data_' + str(args.event)+ '_' + str(args.fromdate) + '_' + str(args.todate) + '.json'
    with open(filename,'w') as outputfile:
            json.dump(final_data,outputfile)