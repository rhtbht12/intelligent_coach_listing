import json
import os
import pandas as pd
import argparse
import numpy as np
import time
from datetime import datetime

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Clevertap data gathering')
    parser.add_argument('--input_dir', type=str,default='app_launched')
    parser.add_argument('--output_dir', type=str,default='applaunched')
    parser.add_argument('--output_filename', type=str,default='clevertap_applaunched.csv')

    args = parser.parse_args()

    if args.output_dir:
        if not os.path.exists(args.output_dir):
            os.makedirs(args.output_dir)


    if os.path.isdir(args.input_dir):
        filenames = [f.path.replace("\\", '/') for f in os.scandir(
                args.input_dir) if f.is_file() and f.path.endswith(('.json'))]

    newDF = pd.DataFrame()
    user_name = []
    user_id = []
    ts = []
    quarter = []

    for f in filenames:
        with open(f) as file:
            data = json.load(file)
            for profile in data['records']:
                if 'name' in profile['profile']:
                    user_name.append(profile['profile']['name'])
                else:
                    user_name.append(np.nan)
                if 'identity' in profile['profile']:
                    user_id.append(profile['profile']['identity'])
                else:
                    user_id.append(np.nan)

                date = datetime.strptime(str(profile['ts']), '%Y%m%d%H%M%S')
                ts.append(date)
                if date.hour in range(5,11):
                    quarter.append('morning')
                elif date.hour in range(11,17):
                    quarter.append('afternoon')
                elif date.hour in range(17,23):
                    quarter.append('evening')
                else:
                    quarter.append('late night')


    d = {'user_name' : user_name, 'user_id' : user_id,'timestamp' : ts, 'quarter' : quarter}

    final_dataframe = pd.DataFrame(d)
    output_file = os.path.join(args.output_dir,args.output_filename).replace('\\','/')
    final_dataframe.to_csv(output_file,index=False)

            






