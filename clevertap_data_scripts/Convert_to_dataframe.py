import json
import os
import pandas as pd
import argparse
import numpy as np

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Clevertap data gathering')
    parser.add_argument('--input_dir', type=str,default='files')
    parser.add_argument('--output_dir', type=str,default='csv')
    parser.add_argument('--output_filename', type=str,default='data.csv')

    args = parser.parse_args()

    if args.output_dir:
        if not os.path.exists(args.output_dir):
            os.makedirs(args.output_dir)


    if os.path.isdir(args.input_dir):
        filenames = [f.path.replace("\\", '/') for f in os.scandir(
                args.input_dir) if f.is_file() and f.path.endswith(('.json'))]

    newDF = pd.DataFrame()
    is_coach = []
    user_name = []
    user_id = []
    currently_enrolled = []
    diet_preference = []
    fitness_level = []
    height = []
    weight = []
    ts = []
    coach_name = []
    tier_name = []
    tier_id = []
    coach_id = []

    for f in filenames:
        with open(f) as file:
            data = json.load(file)
            for profile in data['records']:
                if 'name' in profile['profile']:
                    user_name.append(profile['profile']['name'])
                else:
                    user_name.append(np.nan)
                if 'identity' in profile['profile']:
                    user_id.append(profile['profile']['identity'])
                else:
                    user_id.append(np.nan)

                if 'profileData' in profile['profile']:
                    if 'is_coach' in profile['profile']['profileData']:
                        is_coach.append(profile['profile']['profileData']['is-coach'])
                    else:
                        is_coach.append(np.nan)
                    if 'is-currently-enrolled' in profile['profile']['profileData']:
                        currently_enrolled.append(profile['profile']['profileData']['is-currently-enrolled'])
                    else:
                        currently_enrolled.append(np.nan)
                    if 'diet-preference' in profile['profile']['profileData']:
                        diet_preference.append(profile['profile']['profileData']['diet-preference'])
                    else:
                        diet_preference.append(np.nan)
                    if 'fitness-level' in profile['profile']['profileData']:
                        fitness_level.append(profile['profile']['profileData']['fitness-level'])
                    else:
                        fitness_level.append(np.nan)
                        
                    if 'height' in profile['profile']['profileData']:
                        height.append(profile['profile']['profileData']['height'])
                    else:
                        height.append(np.nan)
                    if 'weight' in profile['profile']['profileData']:
                        weight.append(profile['profile']['profileData']['weight'])
                    else:
                        weight.append(np.nan)
                else:
                    is_coach.append(np.nan)
                    currently_enrolled.append(np.nan)
                    diet_preference.append(np.nan)
                    fitness_level.append(np.nan)
                    height.append(np.nan)
                    weight.append(np.nan)

                if 'ts' in profile:
                    ts.append(profile['ts'])
                else:
                    ts.append(np.nan)
                if 'coach_name' in profile['event_props']:
                    coach_name.append(profile['event_props']['coach_name'])
                else:
                    coach_name.append(np.nan)
                if 'tier_name' in profile['event_props']:
                    tier_name.append(profile['event_props']['tier_name'])
                else:
                    tier_name.append(np.nan)
                if 'tier_id' in profile['event_props']:
                    tier_id.append(profile['event_props']['tier_id'])
                else:
                    tier_id.append(np.nan)
                if 'coach_id' in profile['event_props']:
                    coach_id.append(profile['event_props']['coach_id'])
                else:
                    coach_id.append(np.nan)

    d = {'user_name' : user_name, 'user_id' : user_id, 'is_coach' : is_coach, 'currently_enrolled': currently_enrolled, 'diet_preference': diet_preference, \
            'fitness_level' : fitness_level, 'user_height' : height , 'user_weight' : weight,'timestamp' : ts, 'coach_name' : coach_name, 'coach_id': coach_id, \
            'tier_name' : tier_name, 'tier_id' : tier_id}

    final_dataframe = pd.DataFrame(d)
    output_file = os.path.join(args.output_dir,args.output_filename).replace('\\','/')
    final_dataframe.to_csv(output_file,index=False)

            






