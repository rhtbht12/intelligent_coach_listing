import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
import sqlalchemy as sql


class fittrdb:

    def __init__(self):
        self.db_host = "fittr-production-db-cluster.cluster-c0jgn9okc61m.eu-west-3.rds.amazonaws.com"
        self.sql_driver = "/usr/local/lib/libodbc.2.dylib"
        self.sql_db_name = "fittr_production_mumbai"
        self.sql_username = "fittr_db_123"
        self.sql_password = "fittr_master_123#"

    def create_engine(self):
        self.engine = sql.create_engine("mysql+pymysql://%s:%s@%s:3306/%s"
                                   % (str(self.sql_username),str(self.sql_password),str(self.db_host),str(self.sql_db_name)), echo=False, pool_size=5, pool_recycle=3600)


    def execute(self,query):
        data = pd.read_sql(query,con=self.engine)
        return data



class squatsdb:
    
    def __init__(self):
        self.db_host = "squats-production-db-cluster.cluster-c0jgn9okc61m.eu-west-3.rds.amazonaws.com"
        self.sql_driver = "/usr/local/lib/libodbc.2.dylib"
        self.sql_db_name = "squatsin_squats"
        self.sql_username = "squats_user"
        self.sql_password = "SquatSIndia321#$"

    def create_engine(self):
        self.engine = sql.create_engine("mysql+pymysql://%s:%s@%s:3306/%s"
                                   % (str(self.sql_username),str(self.sql_password),str(self.db_host),str(self.sql_db_name)), echo=False, pool_size=5, pool_recycle=3600)


    def execute(self,query):
        data = pd.read_sql(query,con=self.engine)
        return data